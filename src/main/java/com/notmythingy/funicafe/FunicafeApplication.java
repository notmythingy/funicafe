package com.notmythingy.funicafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunicafeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunicafeApplication.class, args);
	}

}

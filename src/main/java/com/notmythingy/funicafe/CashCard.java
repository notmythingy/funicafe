package com.notmythingy.funicafe;

import org.springframework.data.annotation.Id;

public record CashCard(@Id Long id, String firstName, String lastName, String email, Double amount) {
}

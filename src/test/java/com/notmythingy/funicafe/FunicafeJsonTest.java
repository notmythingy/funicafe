package com.notmythingy.funicafe;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@JsonTest
public class FunicafeJsonTest {

    @Autowired
    private JacksonTester<CashCard> json;

    @Test
    public void cashCardSerializationTest() throws IOException {
        CashCard cashCard = new CashCard(999L, "Olli","Oppilas", "olli.oppilas@helsinki.fi", 123.45);
        assertThat(json.write(cashCard)).isStrictlyEqualToJson("expected.json");
        assertThat(json.write(cashCard)).hasJsonPathNumberValue("@.id");
        assertThat(json.write(cashCard)).extractingJsonPathNumberValue("@.id").isEqualTo(999);
        assertThat(json.write(cashCard)).hasJsonPathStringValue("@.firstName");
        assertThat(json.write(cashCard)).extractingJsonPathStringValue("@.firstName").isEqualTo("Olli");
        assertThat(json.write(cashCard)).hasJsonPathStringValue("@.lastName");
        assertThat(json.write(cashCard)).extractingJsonPathStringValue("@.lastName").isEqualTo("Oppilas");
        assertThat(json.write(cashCard)).hasJsonPathStringValue("@.email");
        assertThat(json.write(cashCard)).extractingJsonPathStringValue("@.email").isEqualTo("olli.oppilas@helsinki.fi");
        assertThat(json.write(cashCard)).hasJsonPathNumberValue("@.amount");
        assertThat(json.write(cashCard)).extractingJsonPathNumberValue("@.amount").isEqualTo(123.45);
    }

    @Test
    public void cashCardDeserializationTest() throws IOException {
        String expected = """
                {
                    "id":999,
                    "firstName":"Olli",
                    "lastName":"Oppilas",
                    "email":"olli.oppilas@helsinki.fi",
                    "amount":123.45
                }
                """;
        assertThat(json.parse(expected)).isEqualTo(new CashCard(
                999L,
                "Olli",
                "Oppilas",
                "olli.oppilas@helsinki.fi",
                123.45));
    }
}
